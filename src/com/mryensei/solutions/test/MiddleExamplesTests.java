package com.mryensei.solutions.test;
import org.junit.Test;

import com.mryensei.solutions.MiddleCharacter;

import static org.junit.Assert.assertEquals;

public class MiddleExamplesTests {
  @Test
  public void evenTests() {
    assertEquals("es", MiddleCharacter.getMiddle("test"));
    assertEquals("dd", MiddleCharacter.getMiddle("middle"));
  }
  
  @Test
  public void oddTests() {
    assertEquals("t", MiddleCharacter.getMiddle("testing"));
    assertEquals("A", MiddleCharacter.getMiddle("A"));
  }
}