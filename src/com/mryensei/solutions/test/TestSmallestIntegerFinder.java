package com.mryensei.solutions.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.mryensei.solutions.fundamentals.SmallestIntegerFinder;

public class TestSmallestIntegerFinder {

	@Test
	public void testFindSmallestInt() {
		int expected = 11;
        int actual = SmallestIntegerFinder.findSmallestInt(new int[]{78,56,232,12,11,43});
        assertEquals(expected, actual);
	}
	
	@Test
    public void example2(){
        int expected = -33;
        int actual = SmallestIntegerFinder.findSmallestInt(new int[]{78,56,-2,12,8,-33});
        assertEquals(expected, actual);
    }
    
    @Test
    public void example3(){
        int expected = Integer.MIN_VALUE;
        int actual = SmallestIntegerFinder.findSmallestInt(new int[]{0,Integer.MIN_VALUE,Integer.MAX_VALUE});
        assertEquals(expected, actual);
    }

    

	@Test
	public void testFindSmallestIntUsingCollections() {
		int expected = 11;
        int actual = SmallestIntegerFinder.findSmallestIntUsingCollections(new int[]{78,56,232,12,11,43});
        assertEquals(expected, actual);
	}
	
	@Test
    public void example2UsingCollections(){
        int expected = -33;
        int actual = SmallestIntegerFinder.findSmallestIntUsingCollections(new int[]{78,56,-2,12,8,-33});
        assertEquals(expected, actual);
    }
    
    @Test
    public void example3UsingCollections(){
    	//no funciona aqui
        int expected = Integer.MIN_VALUE;
        int actual = SmallestIntegerFinder.findSmallestIntUsingCollections(new int[]{0,Integer.MIN_VALUE,Integer.MAX_VALUE});
        assertEquals(expected, actual);
    }
    


	@Test
	public void testFindSmallestIntUsingStream() {
		int expected = 11;
        int actual = SmallestIntegerFinder.findSmallestIntUsingStream(new int[]{78,56,232,12,11,43});
        assertEquals(expected, actual);
	}
	
	@Test
    public void example2UsingStream(){
        int expected = -33;
        int actual = SmallestIntegerFinder.findSmallestIntUsingStream(new int[]{78,56,-2,12,8,-33});
        assertEquals(expected, actual);
    }
    
    @Test
    public void example3UsingStream(){
    	//no funciona aqui
        int expected = Integer.MIN_VALUE;
        int actual = SmallestIntegerFinder.findSmallestIntUsingStream(new int[]{0,Integer.MIN_VALUE,Integer.MAX_VALUE});
        assertEquals(expected, actual);
    }
    
    
    
    
    
    
    
    
}
