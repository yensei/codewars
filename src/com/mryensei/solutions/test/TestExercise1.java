package com.mryensei.solutions.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.mryensei.solutions.ExerciseSolution1;

public class TestExercise1 {
	
	@Test
    public void test() {
      assertEquals(23, new ExerciseSolution1().solution(10));
    }

}
