package com.mryensei.solutions.fundamentals;

import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

/**
Description:

Find the smallest integer in the array.

Given an array of integers your solution should find the smallest integer. For example:
Given [34, 15, 88, 2] your solution will return 2
Given [34, -345, -1, 100] your solution will return -345

You can assume, for the purpose of this kata, that the supplied array will not be empty.
 * @author julio
 *
 */

public class SmallestIntegerFinder {
	
	//La forma tradicional
	public static int findSmallestInt(int[] args) {

		int minValue = args[0];
		for (int i = 0; i < args.length; i++) {
			if(minValue> args[i]){
				minValue = args[i];
				i =0;
			}
		}
		
		return minValue;
    }
	
	
	public static int findSmallestIntUsingCollections(int[] args) {
		
		List<Integer> l = (List)Arrays.asList(args);
		
		try{
			Integer n = (Integer) Collections.min(l);
			return n;
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return -1;
	}
	

	public static int findSmallestIntUsingStream(int[] args) {
		IntStream stream = IntStream.of(args) ;
		return stream.min().getAsInt();
	}
	
}
