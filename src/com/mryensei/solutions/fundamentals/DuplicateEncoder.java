/**
 * The goal of this exercise is to convert a string to a new string where each character in the new string 
 * is "(" if that character appears only once in the original string, or ")" 
 * if that character appears more than once in the original string. Ignore capitalization when determining 
 * if a character is a duplicate.
 * 
 * Examples
* "din"      =>  "((("
* "recede"   =>  "()()()"
* "Success"  =>  ")())())"
* "(( @"     =>  "))(("
* 
* Notes
* Assertion messages may be unclear about what they display in some languages. If you read "...It Should encode XXX", the "XXX" is the expected result, not the input!
*/

package com.mryensei.solutions.fundamentals;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class DuplicateEncoder {	
	public static String encode(String word) {
		char[] array = word.toLowerCase().toCharArray();
		String res="";
		for(int i =0; i< word.length();i++) {
			int c =0;
			for(int j =0; j< word.length();j++) {
				if(array[i] == array[j]) {
					c++;
				}
			}
			if(c>=2) {
				res+= ")";
			}else {
				res+= "(";
			}
		}	
		
		return res;
	}

	@Test
	public void EncodeTest() {
		assertEquals("(((", DuplicateEncoder.encode("din"));
		assertEquals("()()()", DuplicateEncoder.encode("recede"));
		assertEquals(")())())", DuplicateEncoder.encode("Success"));
		assertEquals("))((", DuplicateEncoder.encode("(( @"));
	}
}

