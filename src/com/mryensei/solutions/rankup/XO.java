package com.mryensei.solutions.rankup;


/**
 * 
 * @author julio
 * Check to see if a string has the same amount of 'x's and 'o's. 
 * The method must return a boolean and be case insensitive. The string can contains any char.
 *
 *	Examples input/output:
 * XO("ooxx")    => true
 * XO("xooxx")   => false
 * XO("ooxXm")   => true
 * XO("zpzpzpp") => true // when no 'x' and 'o' is present should return true
 * XO("zzoo")    => false
 */
public class XO {
	
	public XO(){
		super();
	}
	
	public static boolean getXO (String str) {
		
		char[] array = str.toLowerCase().toCharArray();
		
		int xCount=0,oCount=0;
		
		for (int i = 0; i < array.length; i++) {
			xCount += array[i] == 'x' ? 1 : 0; 
			oCount += array[i] == 'o' ? 1 : 0; 
		}
		
		return xCount == oCount;
	 }
}
