package com.mryensei.solutions.rankup;

public class BitCounting {
	public static int countBits(int n){
		
		String str = Integer.toBinaryString(n);
		return str.length() - str.replaceAll("1", "").length();

	}

}
