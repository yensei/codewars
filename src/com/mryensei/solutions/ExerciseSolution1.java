package com.mryensei.solutions;

/**
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Finish the solution so that it returns the sum of all the multiples of 3 or 5 below the number passed in.

Note: If the number is a multiple of both 3 and 5, only count it once.
Courtesy of ProjectEuler.net
 * @author julio
 *
 */
public class ExerciseSolution1{
	 public int solution(int number) {
		 int sum = 0;
		 
		 for (int i = 0; i < number-1; i++) {
			int n = i+1;//evita el cero 
			
			if(isFiveMultiple(n) || isThreeMultiple(n)) {
				sum += n;
			}
		}
		 return sum;
	  }
	 
	 	 
	 private boolean isFiveMultiple(int n){
		 return n%5 == 0;
	 }
	 
	 private boolean isThreeMultiple(int n){
		 return n%3 == 0;
	 }
}





